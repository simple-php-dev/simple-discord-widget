# Simple Discord Widget

Name says it all really! Simple Discord Widget *(SDW)*  is a custom built widget for inclusion on websites to show your discord server in all it's glory but offering more customisation than the default on on offer. 

## Installing

1.
Add main CSS
```
<link href="../vendor/discord/css/discord.css" rel="stylesheet">
```
2. choose either light
```
<link href="../vendor/discord/css/light.css" rel="stylesheet">
```
or dark theme
```
<link href="../vendor/discord/css/dark.css" rel="stylesheet">
```
3.
Then add the vendor/discord/ folder to ../vendor/ (If you change this make sure you change the path to css)
Edit the discord config with all required changes
```
/vendor/discord/discordConfig.php
```
4.
Then add the link to the JS
```
<script src="../vendor/discord/js/discord.js"></script>
```
5.
Add the required ID "discordWidget" to the colum you want the widget to appear on
```
<div class="col-lg-4" id="discordWidget"></div>
      