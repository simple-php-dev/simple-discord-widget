<!DOCTYPE html>
<html lang="en">

<head>
  <link href="../vendor/discord/css/light.css" rel="stylesheet">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Simple Discord Widget</title>

  <!-- Bootstrap core CSS -->
  <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../vendor/bootstrap/css/custom.css" rel="stylesheet">
  <!-- Discord CSS -->
  <link href="../vendor/discord/css/discord.css" rel="stylesheet">
    <!-- Bootstrap core JavaScript -->
  <!-- Load Fontawesome -->
  <link href="../vendor/bootstrap/css/fontawesome/all.css" rel="stylesheet">

  <!-- Load Jquery & Boostrap JS-->
  <script src="../vendor/jquery/jquery-3.4.1.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../vendor/discord/js/discord.js"></script>

  <script>
    function changeCSS(cssFile, cssLinkIndex) {

    var oldlink = document.getElementsByTagName("link").item(cssLinkIndex);

    var newlink = document.createElement("link");
    newlink.setAttribute("rel", "stylesheet");
    newlink.setAttribute("type", "text/css");
    newlink.setAttribute("href", cssFile);

    document.getElementsByTagName("head").item(0).replaceChild(newlink, oldlink);
    }
  </script>
</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
      <a class="navbar-brand" href="#">Simple Discord Widget</a>

    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1 class="mt-5">Simple Discord Widget</h1>
        <p class="lead">Home grown Discord widget for bootstrap</p>
        <ul class="list-unstyled">
          <li>Bootstrap 4.3.1</li>
          <li>jQuery 3.4.1</li>
          <li>Fontawesome 5.13.0</li>
        </ul>
        <p>
        <a href="#" onclick="changeCSS('../vendor/discord/css/light.css', 0);" class="btn btn-primary">Light Theme</a> 
        <a href="#" onclick="changeCSS('../vendor/discord/css/dark.css', 0);" class="btn btn-primary">Dark Theme</a>
        </p>
      </div>
    </div>
    <div class="row" >
    <div class="col-lg-4" id="discordWidget"></div>
      <div class="col-lg-6">
        <ol>
          <li><p>First add the CSS</p>
              <code><pre><?php echo htmlspecialchars('<link href="../vendor/discord/css/discord.css" rel="stylesheet">');?></pre></code>
              choose either light
              <code><pre><?php echo htmlspecialchars('<link href="../vendor/discord/css/light.css" rel="stylesheet">');?></pre></code>
              or dark theme
              <code><pre><?php echo htmlspecialchars('<link href="../vendor/discord/css/dark.css" rel="stylesheet">');?></pre></code>
            </li>
          <li>
            <p>
              Then add the vendor/discord/ folder to ../vendor/ 
              (<i>If you change this make sure you change the path to css</i>)
            </p>
          </li>
          <li>
            <p>Edit the discord config with all required changes</p>
            <code><pre><?php echo htmlspecialchars('/vendor/discord/discordConfig.php');?></pre></code></li>
          </li>
          <li>
            <p>
            Then add the link to the JS 
            </p>
            <code><pre><?php echo htmlspecialchars('<script src="../vendor/discord/js/discord.js"></script>'); ?></pre></code>
          </li>
          <li>
            <p>Add  the required ID "discordWidget" to the colum you want the widget to appear on </p>
            <code><pre><?php echo htmlspecialchars('<div class="col-lg-4" id="discordWidget"></div>'); ?></pre></code>
          </li>
      </ol>
        
        
        
      </div>
    </div>
  </div>
</body>
</html>
